

# 功能

商城 （分销功能） 微信及小程序支付

NFT数字藏品

实时行情

websocket

区块链 K线图

C2C

在线客服(websocket)

ETH,BTC,USDT（TRC20及OMNI）

资产管理 (收款，转帐等)


会员升级购买

团队等级管理


# 联系

QQ:1 9 9 5 9 4 8 9 @ q q.com


# DEMO 

http://www.ibytecoin.com

==================================

DEMO用户名：test3 密码：123456 登陆

==================================

# 微信

![](https://www.saoker.com/resources/img/qrcode3.jpg)



# APP

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/f1.jpg)

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/f2.jpg)

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/f3.jpg)


# 后台管理

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/admin1.jpg)

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/admin2.jpg)

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/admin3.jpg)

![](https://gitee.com/ouyangyiguang/ibytecoin/raw/master/img/admin4.jpg)